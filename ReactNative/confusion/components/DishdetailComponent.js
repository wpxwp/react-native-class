import React, { Component } from 'react';
import { View, Text, ScrollView, FlatList, StyleSheet, Modal, Button, Alert, PanResponder } from 'react-native';
import { Card, Icon, Input } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { postFavorite, postComment } from '../redux/ActionCreators';
import { Rating } from 'react-native-ratings';
import * as Animatable from 'react-native-animatable';

const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        comments: state.comments,
        favorites: state.favorites
    }
}

const mapDispatchToProps = dispatch => ({
    postFavorite: (dishId) => dispatch(postFavorite(dishId)),
    postComment: (dishId,rating,author,comment) => dispatch(postComment(dishId,rating,author,comment))
})

function RenderDish(props) {

    const dish = props.dish;

    handleViewRef = ref => this.view = ref;

    const recognizeDrag = ({ moveX, moveY, dx, dy }) => {
        if ( dx < -200 )
            return true;
        else
            return false;

    };

    const recognizeComment = ({ moveX, moveY, dx, dy }) => {
        if ( dx > 200 )
            return true;
        else
            return false;

    };

    const panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (e, gestureState) => {
            return true;
        },
        onPanResponderGrant: () => {
            this.view.rubberBand(1000)
                .then(endState => console.log(endState.finished ? 'finished' : 'cancelled'));
        },
        onPanResponderEnd: (e, gestureState) => {
            if (recognizeDrag(gestureState)) {
                Alert.alert(
                    'Add to Favorites?',
                    'Are you sure you wish to add ' + dish.name + ' to your favorites?',
                    [
                        {
                            text: 'Cancel',
                            onPress: () => console.log('Cancel pressed'),
                            style: 'cancel'
                        },
                        {
                            text: 'OK',
                            onPress: () => props.favorite ? console.log('Already Favorite') : props.onPress()
                        }
                    ],
                    { cancelable: false }
                )
                return true;
            } else if (recognizeComment(gestureState)) { props.handleComment(); console.log('drag left'); }
        }
    });

    if (dish != null) {
        return(
            <Animatable.View 
                animation='fadeInDown' 
                duration={2000} 
                delay={1000}
                useNativeDriver={true}
                ref={handleViewRef}
                {...panResponder.panHandlers}
                >
                <Card
                featuredTitle={dish.name}
                image={{ uri: baseUrl + dish.image }}>
                    <Text style={{margin: 10}}>
                        {dish.description}
                    </Text>
                    <View style={styles.icons}>
                        <Icon
                            raised
                            reverse
                            name={ props.favorite ? 'heart' : 'heart-o' }
                            type='font-awesome'
                            color='#f50'
                            onPress={() => props.favorite ? console.log('Already Favorite') : props.onPress() }
                            />
                        <Icon
                            raised
                            reverse
                            name={ 'pencil' }
                            type='font-awesome'
                            color='#512da7'
                            onPress={() => props.makeComment()}
                            accessibilityLabel='Add Your Comment'
                            />
                    </View>
                </Card>
            </Animatable.View>
        );
    }
    else {
        return(<View></View>);
    }
}

function RenderComments(props) {
    const comments = props.comments;

    const renderCommentItem = ({ item, index }) => {
        return(
            <View key={index} style={{margin: 10}}>
                <Text style={{fontSize: 14}}>{item.comment}</Text>
                <Text style={{fontSize: 12}}>{item.rating} Stars</Text>
                <Text style={{fontSize: 12}}>{'-- ' + item.author + ', ' + item.date}</Text>
            </View>
        );

    }

    return(
        <Animatable.View animation='fadeInUp' duration={2000} delay={1000}>
            <Card title="Comments">
                <FlatList
                    data={comments}
                    renderItem={renderCommentItem}
                    keyExtractor={item => item.id.toString()}
                    />
            </Card>
        </Animatable.View>
    )
}

class Dishdetail extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userRating: 1,
            author: '',
            comment: '',
            showModal: false
        }
    }

    toggleModal() {
        this.setState({ showModal: !this.state.showModal })
    }

    ratingCompleted = (setRating) => {
        // console.log("New rating is: " + setRating);
        this.setState({ userRating: setRating });
    }

    handleComment(dishId) {
        this.toggleModal();
    }

    resetForm() {
        this.setState({
            userRating: 1,
            author: '',
            comment: ''          
        });
    }

    markFavorite(dishId) {
        this.props.postFavorite(dishId);   
    }

    addComment(dishId) {
        console.log("dish ID is: " + dishId);
        console.log("states: " + JSON.stringify(this.state));
        this.props.postComment(dishId,this.state.userRating,this.state.author,this.state.comment);
        this.toggleModal();
    }

    static navigationOptions = {
        title: 'Dish Details'
    };

    render() {

        const dishId = this.props.route.params.dishId;

        return(
            <ScrollView>
                <RenderDish 
                    dish={this.props.dishes.dishes[+dishId]}
                    favorite={this.props.favorites.some(el => el === dishId)}
                    onPress={() => this.markFavorite(dishId)}
                    makeComment={() => this.handleComment(dishId)}
                    handleComment={() => this.handleComment(dishId)}
                    />
                <RenderComments comments={this.props.comments.comments.filter((comment) => comment.dishId === dishId)} />
                <Modal
                    animationType={'slide'}
                    transparent={false}
                    visible={this.state.showModal}
                    onDismiss={() => {this.toggleModal(); this.resetForm()}}
                    onRequestClose={() => {this.toggleModal(); this.resetForm()}}
                    >
                    <View style={styles.modal}>
                        <Text style={styles.modalTitle}>Your Comment</Text>
                        <Rating showRating fractions="{0}" startingValue="{4}" onFinishRating={this.ratingCompleted} />
                        <View style={styles.formRow}>
                            <Input
                                placeholder="Author"
                                leftIcon={{ type: 'font-awesome', name: 'user-o' }}
                                leftIconContainerStyle={{paddingRight:20}}
                                style={styles.formInput}
                                value={this.state.author}
                                onChangeText={value => this.setState({ author: value })}
                                />

                        </View>
                        <View style={styles.formRow}>
                            <Input
                                placeholder="Comment"
                                leftIcon={{ type: 'font-awesome', name: 'comment-o' }}
                                leftIconContainerStyle={{paddingRight:20}}
                                style={styles.formInput}
                                value={this.state.comment}
                                onChangeText={value => this.setState({ comment: value })}
                                />
                        </View>
                        <View style={styles.formRow}>
                            <Button
                                onPress={() => {this.addComment(dishId); this.resetForm }}
                                color='#512DA8'
                                title='Add Comment'
                                />
                        </View>
                        <View style={styles.formRow}>
                            <Button
                                style={styles.addButton}
                                onPress={() => { this.toggleModal(); this.resetForm() }}
                                color='#8A8A8a'
                                title='Close'
                                />
                        </View>
                    </View>
                </Modal>            
            </ScrollView>
            
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dishdetail);

const styles=StyleSheet.create({
    icons: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    addButton: {
        margin: 10 
    },
    formRow: {
        margin: 10
    },
    formLabel: {
        fontSize: 18,
        flex: 2
    },
    formItem: {
        flex: 1
    },
    formInput: {
        borderColor: 'black',
        borderBottomWidth: 1,
        height: 40
    },
    modal: {
        justifyContent: 'center',
        margin: 20
    },
    modalTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        backgroundColor: '#512DA8',
        textAlign: 'center',
        color: 'white',
        marginBottom: 20
    },
    modalText: {
        fontSize: 18,
        margin: 10
    }
});