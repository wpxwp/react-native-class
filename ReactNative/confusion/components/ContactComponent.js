import React, { Component } from 'react';
import { View, Text, Linking } from 'react-native';
import { Card, Icon } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';

function RenderItem() {

    return(
        <Animatable.View animation='fadeInDown' duration={2000} delay={1000} useNativeDriver={true}>
            <Card 
                title='Contact Information'
                >
                <Text 
                    style={{margin:10}}
                    >{
    `121, Clear Water Bay Road
    Clear Water Bay, Kowloon
    HONG KONG`
                    }</Text>
                <Text
                    style={{margin:10, textDecorationLine: "underline"}}
                    onPress={() => {Linking.openURL('tel:+852 1234 5678')}}
                    >{`Tel: +852 1234 5678`}</Text>
                <Text 
                    style={{margin:10}}
                    >{`Fax: +852 8765 4321`}</Text>
                <Text 
                    style={{margin:10, textDecorationLine: "underline"}}
                    onPress={() => {Linking.openURL('mailto:confusion@food.net')}}
                    >{`Email:confusion@food.net`}
                </Text>
            </Card>
        </Animatable.View>
    );
}
class Contact extends Component {

    static navigationOptions = {
        title: 'Contact'
    };

    render() {
        return(
            <View>
                <RenderItem/>
            </View>
        );
    }
}

export default Contact;